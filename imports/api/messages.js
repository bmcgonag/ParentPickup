import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Messages = new Mongo.Collection('messages');

Messages.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.message' (entityId, messageText, imageData, intendedRecipient, isGroupRecipient) {
        check(entityId, String);
        check(messageText, String);
        check(imageData, String);
        check(intendedRecipient, String);
        check(isGroupRecipient, Boolean);

        if (!this.userId) {
            throw new Meteor.Error('User is not allowed to add new messages in the system, make sure you are logged in.');
        }

        // need to get the userId and role, and ensure the message is sent properly to the appropriate person / people

    },
    'delete.message' (messageId) {
        check(messageId, String);

        if (!this.userId) {
            throw new Meteor.Error('User is not allowed to add new messages in the system, make sure you are logged in.');
        }

        return Messages.update({ _id: messageId }, {
            $set: {
                isDeleted: true,
            }
        });
    },
});