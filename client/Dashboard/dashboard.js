import { ServiceEntities } from "../../imports/api/serviceentities";

Template.dashboard.onCreated(function() {
    this.subscribe("ServiceEntities");
});

Template.dashboard.onRendered(function() {
    
});

Template.dashboard.helpers({
    entityCount: function() {
        return ServiceEntities.find().count();
    },
});

Template.dashboard.events({

});